#${IPM_DATA_PROCESSING_IP}="10.110.20.11"
#${IPM_DATA_PROCESSING_PORT}="80"

$protocol="http://"
$server="${IPM_DATA_PROCESSING_IP}:${IPM_DATA_PROCESSING_PORT}"
$heartBeat="/info/GetHeartBeatStatus"
$uri="${protocol}${server}${heartBeat}"

$app_path="C:\app"
$data_path=$app_path+'\App_Data'
function ProcessLog {
 param( [string]$msg)
 Write-Host (Get-Date).DateTime $msg
 Echo "$((Get-Date).DateTime) $msg" >> "$data_path\iis.log"
}

function CheckHeartBeat{
    param( [string]$uri)
    Try { 
        $HTTP_Request = [System.Net.WebRequest]::Create($uri)
        $HTTP_Response = $HTTP_Request.GetResponse()
        $HTTP_Status = [int]$HTTP_Response.StatusCode
    }Catch { 
        ProcessLog "Unable to connect to the remote server."
        $result=-1
    }

    If ($HTTP_Status -eq 200) {
        ProcessLog "The server is running."
        $serviceStatus=$(Invoke-WebRequest -Uri $uri -UseBasicParsing).Content
        ProcessLog "The Service Status: $serviceStatus"
        If ($serviceStatus -eq '"Okay"'){
            ProcessLog "The service is ready."
            $result=0
        }Else{
            echo "The service isn't ready."
            $result=-1
        }
    }Else {
        ProcessLog "The server may be down, please check!"
            $result=-1
    }

    If ($HTTP_Response -eq $null) { 
        ProcessLog "The HTTP client is closed."
    }
    Else {
        ProcessLog "The HTTP client is closing."
        $HTTP_Response.Close()
        ProcessLog "The HTTP client is closed."
    }
    return $result
}
return CheckHeartBeat $uri