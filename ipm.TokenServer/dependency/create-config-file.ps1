
# Mock Data
#. .\mock-data.ps1

$PATH='C:\app\App_Data'

# Get the template for creating config.xml
$filename=$IPM_APP_TYPE.ToLower()+'-'+$IPM_APP_ROLE.ToLower()+'.tpl'
$content=$(Get-Content -Path ".\config-template\$filename")
$Variables=$content -match '.+{{[A-Z _]+}}'
$ConfigFile="Config.xml"
if ($IPM_APP_TYPE -eq "TokenService"){
  $ConfigFile="Config.ini"
}
foreach ($variable in $Variables){
  if ($variable -match '(\s+){{-LIST (\<\w+\>){{(.+)}}(\</\w+\>)}}'){
    #{{-LIST <Url>{{IPM_DATA_PROCESSING_SLAVES}}</Url>}}
    $Placeholder=$(echo $Matches[0])
    $List=$(Get-Variable $Matches[3]).Value
    $Target_Value=''
    foreach ($item in $List){
      $Target_Value=$Target_Value+$Matches[1]+$Matches[2]+$item+$Matches[4]+"`r`n"
    }
    $content -replace $Placeholder, $Target_Value > "$PATH\$ConfigFile"
  }else{
    $variable -match '{{(?<Name>.+)}}'
    $Placeholder=$(echo $Matches[0])
    $Target_Value=$(Get-Variable $Matches['Name']).Value
    $content -replace $Placeholder, $Target_Value > "$PATH\$ConfigFile"
  }
  $content=$(Get-Content -Path "$PATH\$ConfigFile")
}
(Get-Content -Raw "$PATH\$ConfigFile").Replace("`r`n`r`n", "`r`n") | Set-Content "$PATH\$ConfigFile"
