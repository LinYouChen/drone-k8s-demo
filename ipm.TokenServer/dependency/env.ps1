#Container image information
$global:IMAGE_NAME="ipm"
$global:IMAGE_VERSION="1.0.0"
$global:IMAGE_KIND="testing"

#IPM application version
$global:SOURCE_CODE_VERSION="1.1.3.0"

#IPM database information
$global:IPM_DB_NAME="<DB_NAME>"
$global:IPM_DB_IP="<192.168.50.1>"
$global:IPM_DB_USER="<USER_NAME>"
$global:IPM_DB_PASSWD="<PASSWORD>"

#IPM STDB information
$global:IPM_STDB_NAME="<DB_NAME>"
$global:IPM_STDB_IP="<192.168.50.12>"
$global:IPM_STDB_USER="<USER_NAME>"
$global:IPM_STDB_PASSWD="<PASSWORD>"

#Image Repository Connection Infomation
$global:DOCKER_USERNAME="<Your Username>"
$global:DOCKER_EMAIL="<Your Password>"
$global:DOCKER_TOKEN="<Your Token"

#Private Registry Connection Infomation
$global:REGISTRY_USERNAME="<Your Username"
$global:REGISTRY_PASSWORD="Your Password"
$global:REGISTRY_SERVER="Your Private Registry"