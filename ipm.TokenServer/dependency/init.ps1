#Setup ODBC
Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
cd C:\app

#Run the App
dotnet.exe "Ipm.TokenServer.dll"
