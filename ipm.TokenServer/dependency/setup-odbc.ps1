Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "User" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

Add-OdbcDsn -Name "${IPM_DB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_DB_IP}")
Add-OdbcDsn -Name "${IPM_STDB_NAME}" -DriverName "SQL Server Native Client 11.0" -DsnType "System" -Platform "64-bit" -SetPropertyValue @("Server=${IPM_STDB_IP}")

